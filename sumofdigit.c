#include <stdio.h>
int input()
{
 int n;
 printf("Enter the number:\n");
 scanf("%d",&n);
 return n;
}    
int sumd(int n)
{
 int sum=0,r;
 while(n>0)
 {
  r=n%10;
  sum=sum+r;
  n=n/10;
 }
 return sum;
}
void output(int sum)
{
 printf("The sum of digits=%d\n",sum);
}
int main()
{
 int n,sum;
 n=input();
 sum=sumd(n);
 output(sum); 
 return 0;
}    
